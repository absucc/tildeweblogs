# ~weblogs
## Requirements
- PHP
## Install
1. Download
2. Upload to a PHP server
3. Edit "blog_config.php"
4. Enjoy!
## Post
1. Open "blog_config.php" in a editor
2. Put a "," on the last publication (the post at the bottom of the document)
3. Add something like:
```
[
    "9/4/2021", // Date
    "Hello!", // Title
    "Lorem Ipsum" // Body
]
```
## Delete
To delete an article just leave in blank the three sections of the thing you want to delete.